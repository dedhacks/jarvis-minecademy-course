package com.jcardonne.jarvis.commands;

import org.bukkit.Location;
import org.bukkit.entity.EntityType;
import org.mineacademy.fo.Common;
import org.mineacademy.fo.command.SimpleCommand;

import java.util.List;

public class SpawnEntityCommand extends SimpleCommand {

	public SpawnEntityCommand() {
		super("spawnentity|se"); // '|' pour séparer des alias

		setMinArguments(1);
		setUsage("/{label} <type> or <type> [x] [y] [z]");
		setDescription("Spawns an entity at your or the given location");
	}

	@Override
	protected void onCommand() {
		//se <type> [x] [y] [z]
		//se pig -> spawn pig at your location
		checkConsole(); // return false si la console fait la commande /se

		EntityType entityType = findEnum(EntityType.class, args[0], "&cEntity named {enum} is invalid.");

		checkBoolean(entityType.isAlive() && entityType.isSpawnable(), "&cEntity " + entityType + " is not spawnable.");

		Location location;
		if (args.length == 4) {
			int x = findNumber(1, "&cInvalid X coordinate"); // check si l'args est un number
			int y = findNumber(2, "&cInvalid Y coordinate"); // check si l'args est un number
			int z = findNumber(3, "&cInvalid Z coordinate"); // check si l'args est un number

			location = new Location(getPlayer().getWorld(), x, y, z);
		} else {
			location = getPlayer().getLocation();
		}

		location.getWorld().spawnEntity(location, entityType);
		tell("&aSpawned - " + entityType + " at " + Common.shortLocation(location));
	}

	@Override
	protected List<String> tabComplete() {
		//<type> [x] [y] [z]
		if (isPlayer()) {
			switch (args.length) {
				case 1:
					return completeLastWord(EntityType.values());
				case 2:
					return completeLastWord(getPlayer().getLocation().getBlockX());
				case 3:
					return completeLastWord(getPlayer().getLocation().getBlockY());
				case 4:
					return completeLastWord(getPlayer().getLocation().getBlockZ());
			}
		}
		return super.tabComplete();
	}
}
