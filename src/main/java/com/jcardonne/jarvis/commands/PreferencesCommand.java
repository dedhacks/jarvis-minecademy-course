package com.jcardonne.jarvis.commands;

import com.jcardonne.jarvis.menu.PreferencesMenu;
import org.mineacademy.fo.command.SimpleCommand;

public class PreferencesCommand extends SimpleCommand {

	public PreferencesCommand() {
		super("pref|preferences");
	}

	@Override
	protected void onCommand() {
		checkConsole();
		new PreferencesMenu().displayTo(getPlayer());
		/*
		InventoryDrawer drawer = InventoryDrawer.of(9*3, "User preferences");
		drawer.setItem(0, ItemCreator.of(CompMaterial.DIAMOND,
				"&bShiny Diamond", "Lore Line", "Lore two")
				.glow(true)
				.build().make());
		drawer.setItem(4, new ItemStack(Material.GOLD_SWORD));
		drawer.display(getPlayer());
		*/
	}
}
