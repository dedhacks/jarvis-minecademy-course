package com.jcardonne.jarvis.commands;

import org.bukkit.entity.Player;
import org.mineacademy.fo.PlayerUtil;
import org.mineacademy.fo.TimeUtil;
import org.mineacademy.fo.command.SimpleCommand;
import org.mineacademy.fo.remain.Remain;

public class StatsCommand extends SimpleCommand {

	public StatsCommand() {
		super("stats");

		setUsage("");
	}

	@Override
	protected void onCommand() {
		checkConsole();

		final Player player = getPlayer();
		final long ticksPlayed = PlayerUtil.getStatistic(player, Remain.getPlayTimeStatisticName());
		tell("You played on the server for " + TimeUtil.formatTimeDays(ticksPlayed / 20) + ".");
	}
}
