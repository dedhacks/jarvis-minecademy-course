package com.jcardonne.jarvis.commands;

import com.jcardonne.jarvis.JarvisPlugin;
import org.mineacademy.fo.Common;
import org.mineacademy.fo.command.SimpleCommand;

public class InformationCommand extends SimpleCommand {
	protected InformationCommand() {
		super("info");
	}

	@Override
	protected void onCommand() {
		Common.tell(getPlayer(), "Info:", "Version: " + JarvisPlugin.getInstance().getDescription().getVersion(), "Spigot User:" + "%%__USER__%%");
	}
}
