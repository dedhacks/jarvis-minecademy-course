package com.jcardonne.jarvis.commands;

import com.jcardonne.jarvis.enchant.BlackNovaEnchant;
import com.jcardonne.jarvis.enchant.HideEnchant;
import org.bukkit.entity.Player;
import org.mineacademy.fo.command.SimpleCommand;
import org.mineacademy.fo.menu.model.ItemCreator;
import org.mineacademy.fo.model.SimpleEnchant;
import org.mineacademy.fo.remain.CompMaterial;

public class CustomEnchantsCommand extends SimpleCommand {
	public CustomEnchantsCommand() {
		super("customenchant|ce");
	}

	@Override
	protected void onCommand() {
		checkConsole();
		final Player player = getPlayer();

		player.getInventory().addItem(
				ItemCreator.of(CompMaterial.IRON_SWORD, "Black Sword", "Mystical Black Sword")
						.enchant(new SimpleEnchant(BlackNovaEnchant.getInstance(), 1))
						.build().makeSurvival(),

				ItemCreator.of(CompMaterial.BOW, "Black Bow")
						.enchant(new SimpleEnchant(HideEnchant.getInstance(), 2))
						.build().makeSurvival());


		tell("&6You were give items with custom enchant");
	}
}
