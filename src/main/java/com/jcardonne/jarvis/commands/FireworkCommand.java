package com.jcardonne.jarvis.commands;

import org.bukkit.entity.Firework;
import org.bukkit.entity.Player;
import org.mineacademy.fo.Common;
import org.mineacademy.fo.command.SimpleCommand;

public class FireworkCommand extends SimpleCommand {

	public FireworkCommand() {
		super("firework"); // /firework -> run this command
	}

	@Override
	protected void onCommand() {
		checkConsole();

		final Player player = getPlayer();
		final String oldPrefix = Common.getTellPrefix();
		Common.setTellPrefix("&d[&5Firework&d] ");
		Common.tell(player, "&fFirework launched!");
		player.getWorld().spawn(player.getLocation(), Firework.class);
		Common.setTellPrefix(oldPrefix);
		Common.log("Hello Firework");
	}
}
