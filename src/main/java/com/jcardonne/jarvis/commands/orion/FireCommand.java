package com.jcardonne.jarvis.commands.orion;

import org.bukkit.entity.Player;
import org.mineacademy.fo.command.SimpleCommandGroup;

public class FireCommand extends TargetCommand {

	protected FireCommand(SimpleCommandGroup parent) {
		super(parent, "fire|f");

		setDescription("Set fire on the target (player)");
	}

	@Override
	protected void onCommandFor(Player target) {
		//orion fire <player>

		int timeInSeconds = 5;
		target.setFireTicks(timeInSeconds * 20); // 20 ticks par seconde -> 5 seconds pour 100ticks
		tell(target.getName() + " burn now in &4&lHELL&r (jk) for " + timeInSeconds + " seconds.");
	}
}
