package com.jcardonne.jarvis.commands.orion;

import org.mineacademy.fo.command.SimpleCommandGroup;

public class OrionCommandGroup extends SimpleCommandGroup {

	@Override
	protected void registerSubcommands() {
		registerSubcommand(new StrikeCommand(this)); //this pour définir le parent de la subcommand
		registerSubcommand(new HideCommand(this));
		registerSubcommand(new FireCommand(this));
		registerSubcommand(new ReloadCommand()); //this not necessary since a previous version of foundation
	}

	@Override
	protected String getCredits() {
		return "Coucou toi";
	}
}
