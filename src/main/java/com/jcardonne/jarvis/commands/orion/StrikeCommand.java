package com.jcardonne.jarvis.commands.orion;

import org.bukkit.entity.Player;
import org.mineacademy.fo.command.SimpleCommandGroup;

public class StrikeCommand extends TargetCommand {

	protected StrikeCommand(SimpleCommandGroup parent) {
		super(parent, "strike|s");
		setDescription("Strike lightning at the target (player)");
	}

	@Override
	protected void onCommandFor(Player target) {
		//orion strike <player>

		target.getWorld().strikeLightning(target.getLocation());
		tell("Stroke lightning at " + target.getName());
	}
}
