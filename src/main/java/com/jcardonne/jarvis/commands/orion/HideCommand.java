package com.jcardonne.jarvis.commands.orion;

import org.bukkit.entity.Player;
import org.mineacademy.fo.command.SimpleCommandGroup;

public class HideCommand extends TargetCommand {

	protected HideCommand(final SimpleCommandGroup parent) {
		super(parent, "hide|h");
		setDescription("Strike lightning at the target (player)");
		//setPermission(null); <- tout le monde peut executer la commande avec null en perm
		setPermission("orion.hide");
		setPermissionMessage("Hahah you don't have the right perm");
	}

	@Override
	protected void onCommandFor(final Player target) {
		//orion hide <player>
		checkConsole();
		checkBoolean(!target.getName().equals(getPlayer().getName()), "You can't hide you from yourself, ur weirdo");

		if (target.canSee(getPlayer())) {
			target.hidePlayer(getPlayer());
			tell("Player " + target.getName() + " can no longer see you");
		} else {
			target.showPlayer(getPlayer());
			tell("Player " + target.getName() + " can now see you");
		}
	}
}
