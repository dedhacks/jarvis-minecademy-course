package com.jcardonne.jarvis.commands.orion;

import org.bukkit.entity.Player;
import org.mineacademy.fo.command.SimpleCommandGroup;
import org.mineacademy.fo.command.SimpleSubCommand;

public class TargetCommand extends SimpleSubCommand {

	protected TargetCommand(SimpleCommandGroup parent, String sublabel) {
		super(parent, sublabel);

		setMinArguments(1);
		setUsage("<target>");
	}

	@Override
	protected void onCommand() {
		final Player target = findPlayer(args[0]);

		onCommandFor(target);
	}

	protected void onCommandFor(Player target) {

	}
}
