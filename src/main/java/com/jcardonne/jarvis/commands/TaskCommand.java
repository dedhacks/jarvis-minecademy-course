package com.jcardonne.jarvis.commands;

import com.jcardonne.jarvis.JarvisPlugin;
import org.bukkit.entity.EntityType;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scheduler.BukkitTask;
import org.mineacademy.fo.Common;
import org.mineacademy.fo.command.SimpleCommand;
import org.mineacademy.fo.remain.CompSound;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class TaskCommand extends SimpleCommand {

	private final Map<UUID, BukkitTask> running = new HashMap<>();

	public TaskCommand() {
		super("task"); // /firework -> run this command
		setMinArguments(1);
		setUsage("<sync|async|start|stop");
	}

	@Override
	protected void onCommand() {
		checkConsole();
		final String param = args[0];

		if ("sync".equals(param)) {
			new BukkitRunnable() {

				@Override
				public void run() {
					tell("Spawn Cow (1/2)");
					CompSound.CAT_MEOW.play(getPlayer());
					getPlayer().getWorld().spawnEntity(getPlayer().getLocation(), EntityType.COW);
				}
			}.runTaskLater(JarvisPlugin.getInstance(), 3 * 20);

			//recommanded
			Common.runLater(6 * 20, () -> {
				tell("Spawn Chicken (2/2)");
				CompSound.CHICKEN_HURT.play(getPlayer());
				getPlayer().getWorld().spawnEntity(getPlayer().getLocation(), EntityType.CHICKEN);
			});

		} else if ("async".equals(param)) {
			Common.runLaterAsync(2 * 20, () -> {
				tell("Spawning a zombie async");
				CompSound.ZOMBIE_HURT.play(getPlayer());
				getPlayer().getWorld().spawnEntity(getPlayer().getLocation(), EntityType.ZOMBIE);
			});
		} else if ("start".equals(param)) {
			checkBoolean(!running.containsKey(getPlayer().getUniqueId()), "A task already running");
			running.put(getPlayer().getUniqueId(), Common.runTimer(20, () -> {
				tell("&6Sending timer message");
			}));
		} else if ("stop".equals(param)) {
			checkBoolean(running.containsKey(getPlayer().getUniqueId()), "No task is running for you");
			final BukkitTask task = running.remove(getPlayer().getUniqueId());

			task.cancel();
			tell("&bStop Sending timer message");

		} else {
			tell("Invalid Args");
		}
	}
}
