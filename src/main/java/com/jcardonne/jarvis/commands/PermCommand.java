package com.jcardonne.jarvis.commands;

import com.jcardonne.jarvis.JarvisPlugin;
import org.bukkit.permissions.PermissionAttachment;
import org.mineacademy.fo.command.SimpleCommand;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class PermCommand extends SimpleCommand {

	private final static String DEMO_PERM = "my.simple.perm";
	private final Map<UUID, PermissionAttachment> permissions = new HashMap<>();
	//TO DO Highest coding standarts to only have command execution code in the command class

	public PermCommand() {
		super("perm"); // /perm -> run this command
		setMinArguments(1);
		setUsage("<add|remove>");
		setPermission(null);
	}

	@Override
	protected void onCommand() {
		// <add|remove>
		checkConsole();
		final String param = args[0].toLowerCase();

		if ("add".equals(param)) {
			checkBoolean(!permissions.containsKey(getPlayer().getUniqueId()), "You already have the " + DEMO_PERM + " permission.");
			final PermissionAttachment perm = getPlayer().addAttachment(JarvisPlugin.getInstance(), DEMO_PERM, true);
			permissions.put(getPlayer().getUniqueId(), perm);
			tell("&3Permission " + DEMO_PERM + "&r&l GRANTED");
		} else if ("remove".equals(param)) {
			final PermissionAttachment perm = permissions.remove(getPlayer().getUniqueId());

			checkNotNull(perm, "Run /{label} add first to get the right perm");
			getPlayer().removeAttachment(perm);
			tell("&3Permission " + DEMO_PERM + "&r&l DENIED");

		} else {
			returnInvalidArgs();
		}
	}
}
