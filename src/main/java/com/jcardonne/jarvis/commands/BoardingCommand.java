package com.jcardonne.jarvis.commands;

import com.jcardonne.jarvis.conversation.OnBoardingConversation;
import org.mineacademy.fo.command.SimpleCommand;

public class BoardingCommand extends SimpleCommand {

	public BoardingCommand() {
		super("survey|board");
	}

	@Override
	protected void onCommand() {
		checkConsole();
		new OnBoardingConversation().start(getPlayer());
	}
}
