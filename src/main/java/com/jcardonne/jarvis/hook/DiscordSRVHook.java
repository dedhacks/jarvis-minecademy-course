package com.jcardonne.jarvis.hook;

import github.scarsz.discordsrv.api.events.DiscordGuildMessagePreProcessEvent;
import github.scarsz.discordsrv.dependencies.jda.api.entities.Member;
import github.scarsz.discordsrv.dependencies.jda.api.entities.Message;
import github.scarsz.discordsrv.dependencies.jda.api.entities.TextChannel;
import org.bukkit.GameMode;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.mineacademy.fo.Common;
import org.mineacademy.fo.PlayerUtil;
import org.mineacademy.fo.model.DiscordListener;
import org.mineacademy.fo.remain.Remain;

import java.util.Arrays;
import java.util.List;

public class DiscordSRVHook extends DiscordListener {

	//Secondary thread | Async
	@Override
	protected void onMessageReceived(final DiscordGuildMessagePreProcessEvent event) {
		final TextChannel channel = event.getChannel();
		final Member member = event.getMember();
		final Message message = event.getMessage();
		final String chatMessage = message.getContentDisplay();

		event.setCancelled(true); //remove message Minecraft <- Discord


		// <------------------------------------------------------------>
		//                    Filtering profanity
		// <------------------------------------------------------------>


		final List<String> profanities = Arrays.asList("fuck", "ass");

		for (final String profanity : profanities)
			if (chatMessage.contains(profanity)) {
				message.delete().queue();


				removeAndWarn(message, "Please do not use profanity in this guild!", 2);
				//Same thing, but A is better
				//removeAndWarn("Please do not use profanity in this guild!");

				return;
			}

		// <------------------------------------------------------------>
		//                    Running commands Discord -> Minecraft
		// <------------------------------------------------------------>

		if (chatMessage.equals("/creative")) {
			final String permission = "jarvis.discord.command.creative";
			final Player player = findPlayer(member.getEffectiveName(),"You are not connected to the server ("+member.getEffectiveName()+")!");

			checkBoolean(PlayerUtil.hasPerm(player, permission), "Insufficient permission (" + permission + ")!");

			Common.runLater(() -> player.setGameMode(GameMode.CREATIVE));

			returnHandled("Your gamemode has been changed to creative.");
		}

		// <------------------------------------------------------------>
		//                  Clear Messages
		// <------------------------------------------------------------>

		/*
		if (chatMessage.equals("/nuke")) {
			checkBoolean(hasRole(member, "J-C"), "Only for admin :P");
			for (final Message oldMessage : channel.getIterableHistory().complete()) {
				// final long distanceFromNow = (System.currentTimeMillis() / 1000) - oldMessage.getTimeCreated().toEpochSecond();
				// if (distanceFromNow > 3600 * 24 * 30) delete older than 30 days
				//System.out.println("Queue: " + oldMessage.getContentDisplay());
					oldMessage.delete().complete();
			}
			returnHandled("Your gamemode has been changed to creative.");
		}*/

		if (chatMessage.equals("/clear")) {
			for (final Message oldMessage : channel.getIterableHistory().complete()) {
					oldMessage.delete().queue();
			}
			returnHandled("Chat channel has been cleared.");
		}

		if (channel.getName().equals("vip") && !hasRole(member, "VIP"))
			returnHandled("Only VIP players can post to this channel!");

		if (channel.getName().equals("minecraft"))
			for (final Player player : Remain.getOnlinePlayers())
				Common.tellNoPrefix(player, "&8[&bDiscord&8] &7" + member.getEffectiveName() + ": &f" + chatMessage);
	}

	@EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
	public void onMessageSend(final AsyncPlayerChatEvent event) {
		final Player player = event.getPlayer();
		final String message = event.getMessage();

		sendMessage(player, "jarvis", Common.stripColors(message)); // Adds player name before automatically
		//sendMessage("minecraft", Common.stripColors(message));
	}
	}
