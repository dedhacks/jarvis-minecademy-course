package com.jcardonne.jarvis.utils;

import com.jcardonne.jarvis.tools.KittyAmmo;
import org.bukkit.GameMode;
import org.bukkit.entity.Player;
import org.bukkit.event.Cancellable;
import org.bukkit.inventory.ItemStack;
import org.mineacademy.fo.Common;
import org.mineacademy.fo.PlayerUtil;

public class JarvisUtil {

	public static boolean checkKittyArrow(final Player player, final Cancellable event) {
		final ItemStack arrow = PlayerUtil.getFirstItem(player, KittyAmmo.getInstance().getItem());

		if (arrow == null) {
			Common.tell(player, "&cYou lack a KittyAmmo");
			event.setCancelled(true);
			return false;
		}

		if (player.getGameMode() == GameMode.SURVIVAL) {
			PlayerUtil.takeOnePiece(player, arrow);
		}
		return true;
	}
}
