package com.jcardonne.jarvis.menu;

import com.jcardonne.jarvis.PlayerCache;
import com.jcardonne.jarvis.conversation.ExPrompt;
import com.jcardonne.jarvis.rpg.ClassRegister;
import com.jcardonne.jarvis.rpg.PlayerClass;
import com.jcardonne.jarvis.settings.Settings;
import com.jcardonne.jarvis.tools.*;
import lombok.Getter;
import lombok.Setter;
import org.bukkit.ChatColor;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.inventory.ItemStack;
import org.mineacademy.fo.Common;
import org.mineacademy.fo.ItemUtil;
import org.mineacademy.fo.MathUtil;
import org.mineacademy.fo.menu.Menu;
import org.mineacademy.fo.menu.MenuPagged;
import org.mineacademy.fo.menu.MenuQuantitable;
import org.mineacademy.fo.menu.MenuTools;
import org.mineacademy.fo.menu.button.Button;
import org.mineacademy.fo.menu.button.ButtonConversation;
import org.mineacademy.fo.menu.button.ButtonMenu;
import org.mineacademy.fo.menu.model.ItemCreator;
import org.mineacademy.fo.menu.model.MenuQuantity;
import org.mineacademy.fo.remain.CompColor;
import org.mineacademy.fo.remain.CompMaterial;

import java.util.Arrays;
import java.util.stream.Collectors;

public class PreferencesMenu extends Menu {
	private final Button timeButton;
	private final Button mobEggButton;
	private final Button chatColorButton;
	private final Button toolAxeButton;
	private final Button levelButton;
	private final Button toolsButton;
	private final Button classesButton;

	private final Button EXPButton;

	public PreferencesMenu() {
		setTitle(Settings.Menu.MENU_PREFERENCES_TITLE);
		setSize(9 * 4);
		setSlotNumbersVisible();

		toolAxeButton = new Button() {

			@Override
			public void onClickedInMenu(final Player player, final Menu menu, final ClickType click) {
				DiamondAxeTool.getInstance().give(player);
				player.closeInventory();
			}

			@Override
			public ItemStack getItem() {
				return ItemCreator.of(CompMaterial.STICK, "Get tools").build().make();
			}
		};

		timeButton = new Button() {
			@Override
			public void onClickedInMenu(final Player player, final Menu menu, final ClickType clickType) {
				final boolean isDay = isDay();

				player.getWorld().setFullTime(isDay ? 14000 : 1000);
				restartMenu("&3Set the time to &b" + (isDay ? "night&3." : "day&3."));
			}

			@Override
			public ItemStack getItem() {
				final boolean isDay = isDay();
				return ItemCreator.of(isDay ? CompMaterial.SUNFLOWER : CompMaterial.RED_BED,
						"Change the time",
						"", "Current: " + (isDay ? "Day" : "Night"), "Click to switch between", "the day and the night.")
						.build().make();
			}

			private boolean isDay() {
				return getViewer().getWorld().getFullTime() < 12500;
			}
		};

		mobEggButton = new ButtonMenu(new EggSelectionMenu(), CompMaterial.CREEPER_SPAWN_EGG, "Eggs menu", "", "Click to open egg", "selection menu");
		chatColorButton = new ButtonMenu(new ChatColorSelectionMenu(), CompMaterial.INK_SAC, "Chat Color Menu", "", "Click to select your", "messages colors.");
		levelButton = new ButtonMenu(new LevelMenu(), CompMaterial.BEACON, "Level Menu", "", "Change your level");
		toolsButton = new ButtonMenu(new JarvisToolsMenu(), CompMaterial.GOLDEN_HOE, "Tools Menu");
		classesButton = new ButtonMenu(new ClassSelectionMenu(), CompMaterial.IRON_CHESTPLATE, "Classes Menu");
		EXPButton = new ButtonConversation(new ExPrompt(), CompMaterial.EXPERIENCE_BOTTLE, "EXP Give Prompt");


	}

	@Override
	public ItemStack getItemAt(final int slot) {
		if (slot == 9 * 1 + 4)
			return timeButton.getItem();
		else if (slot == 20)
			return mobEggButton.getItem();
		else if (slot == 24)
			return chatColorButton.getItem();
		else if (slot == 30)
			return levelButton.getItem();
		else if (slot == 32)
			return toolAxeButton.getItem();
		else if (slot == 22)
			return toolsButton.getItem();
		else if (slot == 4)
			return classesButton.getItem();
		else if (slot == 1)
			return EXPButton.getItem();
		return null;
	}

	@Override
	protected String[] getInfo() {
		return null;
		/*return new String[] {
				"This menu contains simple features",
				"for players or administrators to",
				"enhance thei gameplay experience"
		};
		PERMET DE METTRE UN ITEM INDICATIF
		*/
	}

	private final class EggSelectionMenu extends MenuPagged<EntityType> {

		private EggSelectionMenu() {
			super(Arrays.asList(EntityType.values()) // super is deprecated
					.stream()
					.filter((entityType) -> entityType.isSpawnable() && entityType.isAlive() && (entityType == EntityType.SHEEP || CompMaterial.makeMonsterEgg(entityType) != CompMaterial.SHEEP_SPAWN_EGG))
					.collect(Collectors.toList()));

			setTitle(Settings.Menu.MENU_EGGS_TITLE);
		}

		@Override
		protected ItemStack convertToItemStack(final EntityType entityType) {
			return ItemCreator.of(CompMaterial.makeMonsterEgg(entityType), "Spawn " + ItemUtil.bountifyCapitalized(entityType)).build().make();
		}

		@Override
		protected void onPageClick(final Player player, final EntityType entityType, final ClickType clickType) {
			player.getInventory().addItem(ItemCreator.of(CompMaterial.makeMonsterEgg(entityType)).build().make());
			animateTitle("Egg added into your inventory!");
		}

		@Override
		protected String[] getInfo() {
			return new String[]{
					"Click an egg to get it",
					"into your inventory."
			};
		}
	}

	private final class ChatColorSelectionMenu extends MenuPagged<ChatColor> {

		protected ChatColorSelectionMenu() {
			super(PreferencesMenu.this, Arrays.asList(ChatColor.values()) // super is deprecated
					.stream()
					.filter((chatColor) -> chatColor.isColor())
					.collect(Collectors.toList()));

			setTitle(Settings.Menu.MENU_CHATCOLOR_TITLE);
		}


		@Override
		protected ItemStack convertToItemStack(final ChatColor color) {
			return ItemCreator.ofWool(CompColor.fromChatColor(color)).name("Select " + color + ItemUtil.bountifyCapitalized(color.name())).build().make();
		}

		@Override
		protected void onPageClick(final Player player, final ChatColor color, final ClickType clickType) {
			final PlayerCache cache = PlayerCache.getCache(player);
			cache.setColor(color);
			animateTitle("You changed the chat color to " + ItemUtil.bountifyCapitalized(color.name()));
			Common.log(ItemUtil.bountifyCapitalized(color.name()));
		}

		@Override
		protected String[] getInfo() {
			return new String[]{
					"Click an wool the color",
					"on your message."
			};
		}
	}

	private final class LevelMenu extends Menu implements MenuQuantitable {

		@Getter
		@Setter
		private MenuQuantity quantity = MenuQuantity.ONE;

		private final Button quantityButton;
		private final Button levelButton;

		private LevelMenu() {
			super(PreferencesMenu.this);
			setTitle("Level Changer");
			quantityButton = getEditQuantityButton(this);
			levelButton = new Button() {
				@Override
				public void onClickedInMenu(final Player player, final Menu menu, final ClickType click) {
					final PlayerCache cache = PlayerCache.getCache(getViewer());
					final int nextLevel = MathUtil.range(cache.getLevel() + getNextQuantity(click), 1, 64);

					cache.setLevel(nextLevel);
					restartMenu("Modificated");
				}

				@Override
				public ItemStack getItem() {
					final PlayerCache cache = PlayerCache.getCache(getViewer());
					return ItemCreator.of(CompMaterial.POTION,
							"Change your level",
							"", "Current: " + cache.getLevel(),
							"&8(&7Mouse Click&8)",
							"< -{q} +{q} >".replace("{q}", quantity.getAmount() + "")).amount(cache.getLevel() == 0 ? 1 : cache.getLevel()).build().make();
				}
			};
		}

		@Override
		protected String[] getInfo() {
			return new String[]{
					"Click to change",
					"the level."
			};
		}

		@Override
		public ItemStack getItemAt(final int slot) {

			if (slot == getCenterSlot())
				return levelButton.getItem();
			if (slot == getSize() - 5)
				return quantityButton.getItem();
			return null;
		}

		@Override
		public MenuQuantity getQuantity() {
			return quantity;
		}

		@Override
		public void setQuantity(final MenuQuantity newQuantity) {
			this.quantity = newQuantity;
		}
	}

	public final class JarvisToolsMenu extends MenuTools {

		@Override
		protected Object[] compileTools() {
			return new Object[]{
					0, HellFireRocket.getInstance(), 0, DiamondAxeTool.getInstance(), 0, KittyAmmo.getInstance(), 0, StickyKitty.getInstance(), KittyBow.getInstance()
			};
		}

		protected JarvisToolsMenu() {
			setTitle(Settings.Menu.MENU_JARVIS_TOOLS_TITLE);
		}

		@Override
		protected String[] getInfo() {
			return new String[]{
					"Select your tools"
			};
		}
	}

	private final class ClassSelectionMenu extends MenuPagged<PlayerClass> {

		protected ClassSelectionMenu() {
			super(PreferencesMenu.this, ClassRegister.getInstance().getLoadedClasses());
		}

		@Override
		protected ItemStack convertToItemStack(final PlayerClass playerClass) {
			final PlayerCache cache = PlayerCache.getCache(getViewer());

			return ItemCreator.of(playerClass.getIcon(),
					playerClass.getName(),
					"",
					"Click to select",
					"this class.")
					.glow(cache.getPlayerClass() != null && cache.getPlayerClass().getName().equals(playerClass.getName()))
					.build().make();
		}

		@Override
		protected void onPageClick(final Player player, final PlayerClass playerClass, final ClickType clickType) {

			/*if (!PlayerUtil.hasPerm(player, "my.class.perm." + playerClass.getName()) {
				animateTitle("You cannot select this class!");

				return;
			}*/

			final PlayerCache cache = PlayerCache.getCache(player);
			cache.setPlayerClass(playerClass);

			playerClass.applyFor(player);
			restartMenu("&2Selected " + playerClass.getName() + " class!");
		}

		@Override
		protected String[] getInfo() {
			return new String[]{
					"Click the classes to",
					"select them."
			};
		}
	}
}
