package com.jcardonne.jarvis.conversation;

import org.bukkit.conversations.ConversationAbandonedEvent;
import org.bukkit.conversations.ConversationContext;
import org.bukkit.conversations.Prompt;
import org.bukkit.entity.Player;
import org.mineacademy.fo.Valid;
import org.mineacademy.fo.conversation.SimpleConversation;
import org.mineacademy.fo.conversation.SimplePrompt;
import org.mineacademy.fo.remain.CompSound;

public class ExPrompt extends SimplePrompt {

	public ExPrompt() {
		super(false);
	}

	@Override
	protected String getPrompt(final ConversationContext ctx) {
		return "Write the amount of exp levels you want to receive.";
	}

	@Override
	protected boolean isInputValid(final ConversationContext context, final String input) {
		if (!Valid.isInteger(input))
			return false;
		int level = Integer.parseInt(input);
		return level > 0 && level < 9009;
	}

	@Override
	protected String getFailedValidationText(final ConversationContext context, final String invalidInput) {
		return "&cOnly specify a non-zero number.";
	}

	@Override
	protected Prompt acceptValidatedInput(final ConversationContext conversationContext, final String input) {
		final int level = Integer.parseInt(input);
		final Player player = getPlayer(conversationContext);
		player.setLevel(level);
		CompSound.LEVEL_UP.play(player);
		tell(conversationContext, "&6Now you have " + level + " XP levels.");

		return Prompt.END_OF_CONVERSATION;
	}

	@Override
	public void onConversationEnd(final SimpleConversation conversation, final ConversationAbandonedEvent event) {
		//Do more things
	}
}
