package com.jcardonne.jarvis.conversation;

import com.jcardonne.jarvis.settings.Localization;
import org.bukkit.conversations.*;
import org.bukkit.entity.Player;
import org.mineacademy.fo.Common;
import org.mineacademy.fo.FileUtil;
import org.mineacademy.fo.ItemUtil;
import org.mineacademy.fo.TimeUtil;
import org.mineacademy.fo.conversation.SimpleCanceller;
import org.mineacademy.fo.conversation.SimpleConversation;
import org.mineacademy.fo.conversation.SimplePrefix;
import org.mineacademy.fo.conversation.SimplePrompt;
import org.mineacademy.fo.menu.model.ItemCreator;
import org.mineacademy.fo.remain.CompMaterial;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class OnBoardingConversation extends SimpleConversation {

	@Override
	protected Prompt getFirstPrompt() {
		return new NamePrompt();
	}

	@Override
	protected ConversationPrefix getPrefix() {
		final String prefix = "&8[&cAPPLICATION&8]&7 ";
		return new SimplePrefix(prefix);
	}

	@Override
	protected ConversationCanceller getCanceller() {
		return new SimpleCanceller("none", "exit"); // commands who cancel the conversation
	}

	@Override
	protected void onConversationEnd(final ConversationAbandonedEvent event) {
		// event.gracefulExit(); --> will be on true if the last prompt is null, or false if we cancelled it with getCanceller
		if (!event.gracefulExit())
			tell(event.getContext().getForWhom(), getPrefix().getPrefix(event.getContext()) + Localization.Boarding.CONVERSATION_CANCELLED);
	}

	enum Boarding {
		NAME,
		JOIN_REASON,
		FAVORITE_GAME
	}

	private class NamePrompt extends SimplePrompt {


		@Override
		protected String getPrompt(final ConversationContext ctx) {
			return "What is your name?";
		}

		@Override
		protected Prompt acceptValidatedInput(final ConversationContext conversationContext, final String input) {
			conversationContext.setSessionData(Boarding.NAME, input);
			return new JoiningReasonPrompt();
		}
	}

	private class JoiningReasonPrompt extends SimplePrompt {


		@Override
		protected String getPrompt(final ConversationContext ctx) {
			return "What brings you on this server, " + ctx.getSessionData(Boarding.NAME) + "?";
		}

		@Override
		protected Prompt acceptValidatedInput(final ConversationContext conversationContext, final String input) {
			conversationContext.setSessionData(Boarding.JOIN_REASON, input);
			return new PreferencePrompt();
		}
	}

	private class PreferencePrompt extends SimplePrompt {


		@Override
		protected String getPrompt(final ConversationContext ctx) {
			return "Which game do you prefer " + ctx.getSessionData(Boarding.NAME) + "?";
		}

		@Override
		protected Prompt acceptValidatedInput(final ConversationContext conversationContext, final String input) {
			conversationContext.setSessionData(Boarding.FAVORITE_GAME, input);

			final Player player = getPlayer(conversationContext);
			final List<String> lines = new ArrayList<>();

			lines.add(Common.chatLine());
			lines.add("Date:" + TimeUtil.getFormattedDateShort());

			for (final Map.Entry<Object, Object> entry : conversationContext.getAllSessionData().entrySet()) {
				lines.add(ItemUtil.bountifyCapitalized(entry.getKey().toString()) + ": " + entry.getValue().toString());
			}

			FileUtil.write("survey/" + player.getName() + ".txt", lines);
			tell(conversationContext, getPrefix().getPrefix(conversationContext) + "Thanks you for finishing the survey, here is your reward!");
			player.getInventory().addItem(ItemCreator.of(CompMaterial.NETHER_STAR, "&3Crate Key").build().make());
			return Prompt.END_OF_CONVERSATION;
		}
	}
}
