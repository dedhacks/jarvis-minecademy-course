package com.jcardonne.jarvis;

import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;
import org.mineacademy.fo.Common;
import org.mineacademy.fo.RandomUtil;
import org.mineacademy.fo.remain.Remain;

import java.util.Arrays;
import java.util.List;

public class BroadcasterTask extends BukkitRunnable {

	private final List<String> messages = Arrays.asList(
			"Hello World", "Coucou toi",
			"C'est un indien et un juif qui rentre dans un bare"
	);

	@Override
	public void run() {
		final String prefix = "&3[&bBC&3]";
		final String message = RandomUtil.nextItem(messages);
		for (final Player player : Remain.getOnlinePlayers()) {
			Common.tellNoPrefix(player, prefix + " " + message);
		}
	}
}
