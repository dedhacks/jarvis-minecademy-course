package com.jcardonne.jarvis.mysql;

import com.jcardonne.jarvis.PlayerCache;
import lombok.Getter;
import org.bukkit.ChatColor;
import org.mineacademy.fo.collection.SerializedMap;
import org.mineacademy.fo.database.SimpleFlatDatabase;

public class JarvisDatabase extends SimpleFlatDatabase<PlayerCache> {

	@Getter
	private final static JarvisDatabase instance = new JarvisDatabase();

	@Override
	protected void onLoad(final SerializedMap map, final PlayerCache data) {
		final ChatColor color = map.get("Color", ChatColor.class);

		if (color != null)
			data.setColor(color);

		//We must make the bukkit api methods run on the main thread by synchronizing them
		/*
		Common.runLater(() -> {
			Bukkit.getWorld("world").getBlockAt(0,0, 0).setType(Material.FLOWER_POT);
		});*/
	}

	@Override
	protected SerializedMap onSave(final PlayerCache data) {
		final SerializedMap map = new SerializedMap();
		if (data.getColor() != null)
			map.put("Color", data.getColor());

		return map;
	}

	/*@Override
	protected int getExpirationDays() {
		return 90; //90 is the default value
	} */
}
