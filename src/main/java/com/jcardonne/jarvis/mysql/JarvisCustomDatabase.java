package com.jcardonne.jarvis.mysql;

import com.jcardonne.jarvis.PlayerCache;
import org.bukkit.ChatColor;
import org.mineacademy.fo.collection.SerializedMap;
import org.mineacademy.fo.database.SimpleDatabase;

public class JarvisCustomDatabase extends SimpleDatabase {

	@Override
	protected void onConnected() {
		update("CREATE TABLE IF NOT EXISTS {table}(UUID varchar(64), Name text, Data text, Updated bigint)");
	}

	protected void onLoad(final SerializedMap map, final PlayerCache data) {
		final ChatColor color = map.get("Color", ChatColor.class);

		if (color != null)
			data.setColor(color);
	}

	protected SerializedMap onSave(final PlayerCache data) {
		final SerializedMap map = new SerializedMap();
		if (data.getColor() != null)
			map.put("Color", data.getColor());

		return map;
	}
}
