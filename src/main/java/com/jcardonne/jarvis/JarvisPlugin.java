package com.jcardonne.jarvis;

import com.jcardonne.jarvis.boss.BossListener;
import com.jcardonne.jarvis.boss.BossTimedTask;
import com.jcardonne.jarvis.commands.*;
import com.jcardonne.jarvis.commands.orion.OrionCommandGroup;
import com.jcardonne.jarvis.event.DatabaseListener;
import com.jcardonne.jarvis.event.LocaleListener;
import com.jcardonne.jarvis.event.PlayerListener;
import com.jcardonne.jarvis.event.ProjectileListener;
import com.jcardonne.jarvis.hook.DiscordSRVHook;
import com.jcardonne.jarvis.mysql.JarvisDatabase;
import com.jcardonne.jarvis.rank.Rank;
import com.jcardonne.jarvis.rank.RankListener;
import com.jcardonne.jarvis.rank.RankUpTask;
import com.jcardonne.jarvis.rpg.ClassRegister;
import com.jcardonne.jarvis.settings.Localization;
import com.jcardonne.jarvis.settings.Settings;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Cow;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.mineacademy.fo.Common;
import org.mineacademy.fo.command.SimpleCommandGroup;
import org.mineacademy.fo.menu.model.ItemCreator;
import org.mineacademy.fo.model.HookManager;
import org.mineacademy.fo.model.Variables;
import org.mineacademy.fo.plugin.SimplePlugin;
import org.mineacademy.fo.remain.CompMaterial;
import org.mineacademy.fo.remain.Remain;
import org.mineacademy.fo.settings.YamlStaticConfig;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.List;

public class JarvisPlugin extends SimplePlugin {

	private final SimpleCommandGroup commandGroup = new OrionCommandGroup();
	private BroadcasterTask broadcasterTask;
	private BossTimedTask bossTimedTask;
	private RankUpTask rankUpTask;

	@Override
	protected void onPluginStart() {
		auth();

		/*LagCatcher.start("mysql");
		JarvisDatabase.getInstance().connect("localhost", 3306, "plugins", "root", "root", "Jarvis");
		LagCatcher.end("mysql", 0, "Connecting to database to {time} ms.");*/

		Common.ADD_TELL_PREFIX = true;
		Common.setTellPrefix(Settings.Prefix.PREFIX);

		Common.ADD_LOG_PREFIX = true;
		Common.setLogPrefix(Settings.Prefix.PREFIX);

		Common.log("Hello World! Jarvis here");


		//{jarvis_rank} -> the current rank (in colors) {jarvis_rank+} <- space if the placeholder is empty

		HookManager.addPlaceholder("{jarvis_rank}", (player) -> {
			final PlayerCache cache = PlayerCache.getCache(player);
			final Rank rank = cache.getRank();
			return rank.getColor() + rank.getName();
		});

		//Commands --------------------------------------
		registerCommand(new FireworkCommand());
		registerCommand(new SpawnEntityCommand());
		registerCommand(new PermCommand());
		registerCommand(new TaskCommand());
		registerCommand(new PreferencesCommand());
		registerCommand(new RpgCommand());
		registerCommand(new BoardingCommand());
		registerCommand(new BossCommand());
		registerCommand(new CustomEnchantsCommand());
		registerCommand(new StatsCommand());
		registerCommand(new RankCommand());

		//Events ----------------------------------------
		Common.registerEvents(new PlayerListener());
		Common.registerEvents(new DatabaseListener());
		Common.registerEvents(new RankListener());
		Common.registerEvents(new ProjectileListener());
		Common.registerEvents(new BossListener());
		Common.registerEvents(new LocaleListener());

		ClassRegister.getInstance().loadClasses();

		Variables.addVariable("player_deaths", sender -> sender instanceof Player ? PlayerCache.getCache((Player) sender).getRank().getName() + "" : "0");
	}

	@EventHandler
	public void onJoin(final PlayerJoinEvent event) {
		final Player player = event.getPlayer();
		Common.tell(player, "&3Hey &b" + player.getName());
		Common.tellLater(2, player, "&3I hope you're &bwell");
		if (Settings.Join.GIVE_STUFF)
			player.getInventory().addItem(ItemCreator.of(CompMaterial.EMERALD_ORE).build().make());

		final String replaceMessage = Variables.replace("Hello {player} {world} world, from {country_code}, you are a {player_deaths} and you have {player_ping}ms.", player);
		Common.tell(player, replaceMessage);
	}

	@EventHandler
	public void onEntitydammage(final EntityDamageByEntityEvent event) {
		final Entity victim = event.getEntity();
		if (event.getDamager() instanceof Player && victim instanceof Cow && Settings.Explode.EXPLODE_COW) {
			if (Settings.Explode.EXPLODE_COW)
				event.getEntity().getWorld().createExplosion(victim.getLocation(), Settings.Explode.EXPLODE_POWER.floatValue());
		}
	}

	@Override
	public List<Class<? extends YamlStaticConfig>> getSettings() {
		return Arrays.asList(Settings.class, Localization.class);
	}

	@Override
	public SimpleCommandGroup getMainCommand() {
		return commandGroup;
	}

	/* Don't work
	@Override
	public boolean areScriptVariablesEnabled() {
		return true;
	}*/


	// This method is called when you start the plugin AND when you reload it
	@Override
	protected void onReloadablesStart() {
		// Those tasks are cancelled on reload so they are only run once each time
		new BroadcasterTask().runTaskTimer(this, 0, Settings.Broadcast.TIMER.getTimeSeconds());
		new BossTimedTask().runTaskTimer(this, 0, 20 /* this will run every second (20 ticks = 1 second) */);
		//new RankupTask().runTaskTimer(this, 0, /*60 **/ 20 /* ONE SECOND --> you want to increase that number for better performance */);
		//new QuestTask().runTaskTimer(this, 0, 20);

		// Events here are disabled on reload
		//registerEventsIf(new LocaleListener(), Settings.LOG_PLAYERS_LOCALE);

		// Commands here are overriden on reload so they are registered only once
		/*
		if (HookManager.isCitizensLoaded())
			registerCommand(new NpcTestCommand());
		else
			Common.log("Citizens not found, limited functionality available!");

		// Packet listeners are automatically unregistered on reload as well
		if (HookManager.isProtocolLibLoaded())
			ProtocolLibHook.addPacketListeners();
*/
		if (HookManager.isDiscordSRVLoaded())
			registerEvents(new DiscordSRVHook());

		// Save player data to MySQL
		for (final Player player : Remain.getOnlinePlayers()) {
			final PlayerCache cache = PlayerCache.getCache(player);

			JarvisDatabase.getInstance().save(player.getName(), player.getUniqueId(), cache);
		}

		// Save the data.db file
		//DataFile.getInstance().save();

		// Clear the cache in the plugin so that we load it fresh for only players that are online right now,
		// saving memory
		PlayerCache.clearAlldataCache();
	}

	public static String uid = "%%__USER__%%";
	public boolean sts = true;

	public void auth() {
		try {
			final URLConnection localURLConnection = new URL("https://jcardonne.com").openConnection();
			localURLConnection.setRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.95 Safari/537.11");
			localURLConnection.connect();

			final BufferedReader localBufferedReader = new BufferedReader(new InputStreamReader(localURLConnection.getInputStream(), Charset.forName("UTF-8")));

			final StringBuilder localStringBuilder = new StringBuilder();
			String str1;
			while ((str1 = localBufferedReader.readLine()) != null) {
				localStringBuilder.append(str1);
			}
			final String str2 = localStringBuilder.toString();
			if (str2.contains(String.valueOf(uid))) {
				disableLeak();
				return;
			}
			this.sts = true;
		} catch (final IOException localIOException) {
			localIOException.printStackTrace();
			disableNoInternet();
			return;
		}
	}

	public void disableLeak() {
		int x = 0;
		while (x != 5000) {
			Bukkit.broadcastMessage(ChatColor.DARK_RED + "You leaked my plugin, 5k broadcast!");
			x++;
		}
		getServer().getPluginManager().disablePlugin(this);
		sts = false;
	}

	public void disableNoInternet() {
		Bukkit.broadcastMessage(ChatColor.RED + "You don't have a valid internet connection, please connect to the internet for the plugin to work!");
		getServer().getPluginManager().disablePlugin(this);
		sts = false;
	}


}
