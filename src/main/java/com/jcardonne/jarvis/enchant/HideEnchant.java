package com.jcardonne.jarvis.enchant;

import lombok.Getter;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.ProjectileHitEvent;
import org.bukkit.event.entity.ProjectileLaunchEvent;
import org.mineacademy.fo.model.SimpleEnchantment;
import org.mineacademy.fo.remain.CompSound;

public class HideEnchant extends SimpleEnchantment {

	@Getter
	private static final Enchantment instance = new HideEnchant();

	protected HideEnchant() {
		super("hide", 5);
	}

	@Override
	protected void onShoot(final int level, final LivingEntity shooter, final ProjectileLaunchEvent event) {
		if (!(shooter instanceof Player))
			return;

		final Player player = (Player) shooter;
		CompSound.ANVIL_LAND.play(player);
	}

	@Override
	protected void onHit(final int level, final LivingEntity shooter, final ProjectileHitEvent event) {
		/*if (event.getHitEntity() == null || !(event.getHitEntity() instanceof LivingEntity))
			return;
			event.getHitEntity() doesn't work on 1.8~ version


		final LivingEntity hitEntity = event.getHitEntity();

		hitEntity.addPotionEffect(new PotionEffect(PotionEffectType.INVISIBILITY, (level * 3) * 20, 0));
				*/
	}
}
