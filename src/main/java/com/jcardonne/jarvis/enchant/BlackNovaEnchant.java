package com.jcardonne.jarvis.enchant;

import lombok.Getter;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.mineacademy.fo.Common;
import org.mineacademy.fo.model.SimpleEnchantment;
import org.mineacademy.fo.remain.Remain;

public class BlackNovaEnchant extends SimpleEnchantment /*implements Listener*/ {

	@Getter
	private static final Enchantment instance = new BlackNovaEnchant();

	private BlackNovaEnchant() {
		super("Black Nova", 5);
	}

	/*@EventHandler
	public void onInventory(final InventoryClickEvent event) {
		ItemStack item = event.getCursor();

		if (item != null && item.containsEnchantment(this)) {
		//run code here
		}
	}*/

	@Override
	protected void onDamage(final int level, final LivingEntity damager, final EntityDamageByEntityEvent event) {
		if (!((event.getEntity()) instanceof LivingEntity))
			return;

		if (damager instanceof Player) {
			final Player player = (Player) damager;

			final int cooldown = Remain.getCooldown(player, Material.IRON_SWORD);

			if (cooldown > 0) {
				Common.tellNoPrefix(player, "&8[&4RPG&8] &fYour tool is loading, wait " + Common.plural((cooldown / 20) + 1, " second") + ".");
				event.setCancelled(true);
				return;
			}

			Remain.setCooldown(player, Material.IRON_SWORD, 5 * 20);
		}
		final LivingEntity victim = (LivingEntity) event.getEntity();
		victim.setFireTicks(3 * 20);
		victim.setVelocity(damager.getEyeLocation().getDirection().multiply(4).setY(5));
	}
}
