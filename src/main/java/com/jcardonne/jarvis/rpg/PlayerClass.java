package com.jcardonne.jarvis.rpg;

import lombok.Getter;
import org.bukkit.entity.Player;
import org.mineacademy.fo.Common;
import org.mineacademy.fo.collection.SerializedMap;
import org.mineacademy.fo.model.ConfigSerializable;
import org.mineacademy.fo.remain.CompMaterial;
import org.mineacademy.fo.settings.YamlConfig;

@Getter
public class PlayerClass extends YamlConfig implements ConfigSerializable {

	private double damageModifier;
	private double hitCooldown;
	private CompMaterial icon;
	private int bossThrowReduction;

	public PlayerClass(final String className) {
		setHeader("Welcome to the main class setting file");
		loadConfiguration("rpg-class.yml", "classes/" + className + (!className.endsWith(".yml") ? ".yml" : ""));

	}

	@Override
	protected void onLoadFinish() {
		damageModifier = getDouble("Damage_Modifier");
		hitCooldown = getDoubleSafe("Hit_Cooldown");
		icon = getMaterial("Icon");
		bossThrowReduction = getInteger("Boss_Throw_Reduction");
	}

	public void applyFor(final Player player) {
		Common.tell(player, "Applying changes from your class " + getName());
	}

	@Override
	public SerializedMap serialize() {
		final SerializedMap map = new SerializedMap();

		map.put("Damage_Modifier", 2.0);
		map.put("Hit_Cooldown", 2.0);
		map.put("Icon", icon);
		map.put("Name", getName());

		return map;
	}

	public static PlayerClass deserialize(final SerializedMap map) {
		final PlayerClass playerClass = new PlayerClass(map.getString("Name"));

		playerClass.damageModifier = map.getDouble("Damage_Modifier");
		playerClass.hitCooldown = map.getDouble("Hit_Cooldown");
		playerClass.icon = map.getMaterial("Icon");

		return playerClass;
	}

	@Override
	public String getName() {

		return getFileName().replace(".yml", "");
	}
}
