package com.jcardonne.jarvis.boss;

import com.jcardonne.jarvis.boss.Model.Boss;
import com.jcardonne.jarvis.boss.Model.BossEquipment;
import com.jcardonne.jarvis.boss.Model.BossSkill;
import com.jcardonne.jarvis.boss.Model.SpawnedBoss;
import org.bukkit.block.CreatureSpawner;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.LivingEntity;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityCombustEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.event.entity.SpawnerSpawnEvent;
import org.bukkit.inventory.ItemStack;
import org.mineacademy.fo.menu.model.ItemCreator;
import org.mineacademy.fo.model.SimpleEnchant;
import org.mineacademy.fo.remain.CompColor;
import org.mineacademy.fo.remain.CompMaterial;
import org.mineacademy.fo.remain.CompMetadata;

import java.util.List;

public class BossListener implements Listener {

	private ItemCreator.ItemCreatorBuilder buildArmor(final CompMaterial material, final String title, final String... lore) {
		return ItemCreator.of(material, title, lore)
				.enchant(new SimpleEnchant(Enchantment.PROTECTION_ENVIRONMENTAL, 1)).color(CompColor.RED);
	}

	@EventHandler
	public void onCombust(final EntityCombustEvent event) {
		if (event.getEntityType() == EntityType.SKELETON)
			event.setCancelled(true);

		final Boss boss = findBoss(event.getEntity());
		if (boss != null)
			event.setCancelled(true);
	}

	private Boss findBoss(final Entity entity) {
		return BossRegister.getInstance().findBoss(entity);
	}


	@EventHandler
	public void onCreatureDeath(final EntityDeathEvent event) {
		if (event.getEntityType() != EntityType.SKELETON)
			return;

		final Boss boss = findBoss(event.getEntity());
		if (boss != null) {

			final List<ItemStack> drops = event.getDrops();
			final LivingEntity entity = event.getEntity();
			final BossEquipment equipment = boss.getEquipment();

			drops.clear();
			drops.add(buildUndammagedItem(equipment.getHelmet()));
			drops.add(buildUndammagedItem(equipment.getChestplate()));
			drops.add(buildUndammagedItem(equipment.getLeggings()));
			drops.add(buildUndammagedItem(equipment.getBoots()));
			//drops.addAll(Arrays.asList(equipment.getArmorContents()));

			if (boss.getDroppedXP() != null)
				event.setDroppedExp(event.getDroppedExp());

			final Entity vehicle = entity.getVehicle();

			if (vehicle != null)
				vehicle.remove();
		}
	}

	@EventHandler
	public void onEntityDamage(final EntityDamageByEntityEvent event) {
		final Entity damager = event.getDamager();
		final Entity victim = event.getEntity();

		if (!(damager instanceof LivingEntity) || !(victim instanceof LivingEntity))
			return;

		Boss boss = findBoss(damager);

		if (boss != null) {
			final SpawnedBoss spawnedBoss = new SpawnedBoss(boss, (LivingEntity) damager);

			for (final BossSkill skill : boss.getSkills())
				if (skill.checkRun())
					skill.onBossAttack(spawnedBoss, event);
		}

		boss = findBoss(victim);

		if (boss != null) {
			final SpawnedBoss spawnedBoss = new SpawnedBoss(boss, (LivingEntity) victim);

			for (final BossSkill skill : boss.getSkills())
				if (skill.checkRun())
					skill.onBossDamaged(spawnedBoss, event);
		}
	}

	private ItemStack buildUndammagedItem(final ItemCreator.ItemCreatorBuilder item) {
		return item.damage(0).build().makeSurvival();
	}

	@EventHandler
	public void onSpawnerSpawn(final SpawnerSpawnEvent event) {
		final CreatureSpawner spawner = event.getSpawner();

		if (!CompMetadata.hasMetadata(spawner, Boss.BOSS_SPAWNER_TAG))
			return;

		final String bossName = CompMetadata.getMetadata(spawner, Boss.BOSS_SPAWNER_TAG);
		final Boss boss = BossRegister.getInstance().findBoss(bossName);

		if (boss != null) {
			event.setCancelled(true);
			boss.spawn(event.getLocation());
		}

	}

}
