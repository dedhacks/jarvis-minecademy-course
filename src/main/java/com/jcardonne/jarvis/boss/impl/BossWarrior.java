package com.jcardonne.jarvis.boss.impl;

import com.jcardonne.jarvis.boss.Model.Boss;
import com.jcardonne.jarvis.boss.Skill.BlockBossSkill;
import com.jcardonne.jarvis.boss.Skill.LightningBossSkill;
import com.jcardonne.jarvis.boss.Skill.ThrowBossSkill;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.EntityType;
import org.mineacademy.fo.menu.model.ItemCreator;
import org.mineacademy.fo.model.SimpleEnchant;
import org.mineacademy.fo.remain.CompColor;
import org.mineacademy.fo.remain.CompMaterial;

public class BossWarrior extends Boss {

	public BossWarrior() {
		super("Skeleton Warrior", EntityType.SKELETON);
		setCustomName("&4Skeleton Warrior");

		setHealth(20.0);
		setEquipment(buildWarriorArmor(CompMaterial.LEATHER_HELMET, "&4Warrior", "Lore LOL"),
				buildWarriorArmor(CompMaterial.LEATHER_CHESTPLATE, "&4Warrior", "Lore LOL"),
				buildWarriorArmor(CompMaterial.LEATHER_LEGGINGS, "&4Warrior", "Lore LOL"),
				buildWarriorArmor(CompMaterial.LEATHER_BOOTS, "&4Warrior", "Lore LOL"));

		setDroppedXP(500);
		//addPotionEffect(PotionEffectType.INVISIBILITY);
		setPassenger(EntityType.CAVE_SPIDER);

		addSkill(new BlockBossSkill());
		addSkill(new LightningBossSkill());
		addSkill(new ThrowBossSkill());


	}

	private ItemCreator.ItemCreatorBuilder buildWarriorArmor(final CompMaterial material, final String title, final String... lore) {
		return ItemCreator.of(material, title, lore)
				.enchant(new SimpleEnchant(Enchantment.PROTECTION_ENVIRONMENTAL, 1)).color(CompColor.RED);
	}

}
