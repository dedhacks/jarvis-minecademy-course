package com.jcardonne.jarvis.boss;

import com.jcardonne.jarvis.boss.Model.Boss;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;
import org.mineacademy.fo.BlockUtil;
import org.mineacademy.fo.Common;
import org.mineacademy.fo.RandomUtil;

public class BossTimedTask extends BukkitRunnable {
	@Override
	public void run() {
		final int radius = 30;

		for (final World world : Bukkit.getWorlds()) {
			int spawnedInThisWorld = 0;

			//if (world.hasStorm()) //make spawn only on storm
			for (final Player player : world.getPlayers()) {

				/*for (final Location outerSphericalLocation : BlockUtil.getSphere(player.getLocation(), radius, true))
					CompParticle.VILLAGER_HAPPY.spawn(outerSphericalLocation);
					Visualisation - do lag on the server
					*/

				if (spawnedInThisWorld > 0 || getBossesInWorld(world) > 1)
					break;

				if (getBossesInRadius(player, radius) > 0)
					continue;

				if (!RandomUtil.chanceD(0.01))
					continue;

				final Location randomLocation = RandomUtil.nextLocation(player.getLocation(), radius, true);
				final int highestY = BlockUtil.findHighestBlockNoSnow(randomLocation);
				if (highestY != -1) {
					randomLocation.setY(highestY);
					spawnedInThisWorld++;

					spawnOneRandomBoss(randomLocation);
				}
			}
		}
	}

	private int getBossesInWorld(final World world) {
		final BossRegister register = BossRegister.getInstance();
		int foundEntities = 0;

		for (final Entity worldEntity : world.getLivingEntities())
			if (register.findBoss(worldEntity) != null)
				foundEntities++;
		//foundEntities += (register.findBoss(worldEntity) != null ? 1 : 0); //cursed for var++;
		return foundEntities;

	}

	private int getBossesInRadius(final Player player, final int radius) {
		final BossRegister register = BossRegister.getInstance();
		int foundEntities = 0;
		for (final Entity nearbyEntity : player.getNearbyEntities(radius, player.getWorld().getMaxHeight(), radius))
			if (register.findBoss(nearbyEntity) != null)
				foundEntities++;

		return foundEntities;
		//foundEntities += (register.findBoss(worldEntity) != null ? 1 : 0); //cursed for var++;		return foundEntities;
	}

	private void spawnOneRandomBoss(final Location spawnLocation) {
		final BossRegister register = BossRegister.getInstance();

		final Boss randomBoss = RandomUtil.nextItem(register.getBosses());
		if (randomBoss != null)
			Common.log("Spawning " + randomBoss.getName() + " at " + Common.shortLocation(spawnLocation));
		randomBoss.spawn(spawnLocation);
	}
}
