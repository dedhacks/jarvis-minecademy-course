package com.jcardonne.jarvis.boss.Model;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import org.bukkit.Location;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.LivingEntity;
import org.bukkit.inventory.EntityEquipment;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.mineacademy.fo.Common;
import org.mineacademy.fo.Valid;
import org.mineacademy.fo.collection.StrictList;
import org.mineacademy.fo.menu.model.ItemCreator;
import org.mineacademy.fo.remain.CompMetadata;
import org.mineacademy.fo.remain.Remain;
import org.mineacademy.fo.remain.nbt.NBTEntity;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@RequiredArgsConstructor(access = AccessLevel.PROTECTED)
@Setter(AccessLevel.PROTECTED)
public abstract class Boss {

	public static final String BOSS_TAG = "JarvisBoss";
	public static final String BOSS_SPAWNER_TAG = "JarvisBossSpawner";
	private final String name;
	private final EntityType type;

	private String customName;
	private Double health;
	private BossEquipment equipment;
	private Integer droppedXP;
	private List<BossPotionEffect> potionEffects = new ArrayList<>();
	private EntityType passenger;
	private StrictList<BossSkill> skills = new StrictList<>();

	public final void spawn(final Location location) {
		Valid.checkBoolean(type.isAlive() && type.isSpawnable(), "The boss type must be alive and spawnable!");
		final LivingEntity entity = (LivingEntity) location.getWorld().spawnEntity(location, EntityType.SKELETON);

		Remain.setCustomName(entity, name);

		if (health != null)
			entity.setHealth(health);

		if (this.equipment != null) {
			final EntityEquipment equipment = entity.getEquipment();

			equipment.setHelmet(this.equipment.getHelmet().build().makeSurvival());
			equipment.setChestplate(this.equipment.getChestplate().build().makeSurvival());
			equipment.setLeggings(this.equipment.getLeggings().build().makeSurvival());
			equipment.setBoots(this.equipment.getBoots().build().makeSurvival());
		}

		if (potionEffects != null)
			for (final BossPotionEffect potionEffects : potionEffects)
				entity.addPotionEffect(new PotionEffect(potionEffects.getPotion(), Integer.MAX_VALUE, potionEffects.getAmplifier()));

		if (passenger != null) {
			final LivingEntity spawnPassenger = (LivingEntity) location.getWorld().spawnEntity(location, passenger);
			spawnPassenger.setPassenger(entity);
		}

		CompMetadata.setMetadata(entity, BOSS_TAG, name);
		final NBTEntity nbtEntity = new NBTEntity(entity);
		nbtEntity.setString(BOSS_TAG, name);
	}

	protected final void setEquipment(final ItemCreator.ItemCreatorBuilder helmet,
	                                  final ItemCreator.ItemCreatorBuilder chestplate,
	                                  final ItemCreator.ItemCreatorBuilder leggings,
	                                  final ItemCreator.ItemCreatorBuilder boots
	) {
		this.equipment = new BossEquipment(helmet, chestplate, leggings, boots);
	}


	public BossEquipment getEquipment() {
		return equipment;
	}


	//on fait deux fonctions pour que dans le cas où on a un dossier on ne veut pas de couleur
	public final String getName() {
		return Common.stripColors(name);
	}

	private String getCustomName() {
		return Common.colorize(customName);
	}


	public final Double getHealth() {
		return health;
	}

	public EntityType getPassenger() {
		return passenger;
	}


	protected final void addPotionEffect(final PotionEffectType potion) {
		potionEffects.add(new BossPotionEffect(potion, 0));
	}

	protected final void addPotionEffect(final PotionEffectType potion, final int amplifier) {
		potionEffects.add(new BossPotionEffect(potion, amplifier));
	}


	public List<BossSkill> getSkills() {
		return Collections.unmodifiableList(skills.getSource());
	}

	public final void addSkill(final BossSkill skill) {
		skills.add(skill);
	}


	public final Integer getDroppedXP() {
		return droppedXP;
	}

	public final EntityType getType() {
		return type;
	}

	@Override
	public boolean equals(final Object obj) {
		return obj instanceof Boss && ((Boss) obj).getName().equals(this.getName());
	}


}
