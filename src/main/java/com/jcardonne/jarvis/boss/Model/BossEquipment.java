package com.jcardonne.jarvis.boss.Model;

import lombok.Data;
import org.mineacademy.fo.menu.model.ItemCreator;

@Data // --> constructor, getter, and even setter
public class BossEquipment {

	private final ItemCreator.ItemCreatorBuilder helmet, chestplate, leggings, boots;

}
