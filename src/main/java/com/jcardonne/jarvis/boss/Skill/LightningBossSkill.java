package com.jcardonne.jarvis.boss.Skill;

import com.jcardonne.jarvis.boss.Model.BossSkill;
import com.jcardonne.jarvis.boss.Model.SpawnedBoss;
import org.bukkit.GameMode;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.mineacademy.fo.Common;
import org.mineacademy.fo.MathUtil;
import org.mineacademy.fo.RandomUtil;

public class LightningBossSkill extends BossSkill {

	public LightningBossSkill() {
		super("Strike Player");
	}

	@Override
	public void onBossDamaged(final SpawnedBoss spawnedBoss, final EntityDamageByEntityEvent event) {
		if (RandomUtil.chance(50) && event.getDamager() instanceof Player) {
			event.setCancelled(true);

			final Player attacker = (Player) event.getDamager();


			Common.tell(attacker, "&6The Boss " + spawnedBoss.getBoss().getName() + "want you to shine!");
			attacker.getWorld().strikeLightningEffect(attacker.getLocation());
			if (attacker.getGameMode() == GameMode.SURVIVAL)
				attacker.setHealth(MathUtil.range(attacker.getHealth() - 4, 0, Integer.MAX_VALUE));
		}
	}
}
