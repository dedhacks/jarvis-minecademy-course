package com.jcardonne.jarvis.boss.Skill;

import com.jcardonne.jarvis.boss.Model.BossSkill;
import com.jcardonne.jarvis.boss.Model.SpawnedBoss;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.mineacademy.fo.Common;
import org.mineacademy.fo.RandomUtil;
import org.mineacademy.fo.remain.CompParticle;
import org.mineacademy.fo.remain.CompSound;

public class BlockBossSkill extends BossSkill {

	public BlockBossSkill() {
		super("Block Attack");
	}

	@Override
	public void onBossDamaged(final SpawnedBoss spawnedBoss, final EntityDamageByEntityEvent event) {
		if (RandomUtil.chance(50) && event.getDamager() instanceof Player) {
			event.setCancelled(true);

			final Player attacker = (Player) event.getDamager();

			Common.tell(attacker, "&6The Boss " + spawnedBoss.getBoss().getName() + "has blocked your attack!");
			CompSound.ANVIL_LAND.play(attacker);
			CompParticle.CLOUD.spawn(spawnedBoss.getEntity().getLocation().add(0, 3, 0));
		}
	}
}
