package com.jcardonne.jarvis.boss.Skill;

import com.jcardonne.jarvis.PlayerCache;
import com.jcardonne.jarvis.boss.Model.BossSkill;
import com.jcardonne.jarvis.boss.Model.SpawnedBoss;
import com.jcardonne.jarvis.rpg.PlayerClass;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.util.Vector;
import org.mineacademy.fo.Common;
import org.mineacademy.fo.RandomUtil;

public class ThrowBossSkill extends BossSkill {

	public ThrowBossSkill() {
		super("Throw Player");
		setDelaySeconds(5);
	}

	@Override
	public void onBossDamaged(final SpawnedBoss spawnedBoss, final EntityDamageByEntityEvent event) {
		if (RandomUtil.chance(50) && event.getDamager() instanceof Player) {

			final Player attacker = (Player) event.getDamager();
			final PlayerCache cache = PlayerCache.getCache(attacker);
			final PlayerClass playerClass = cache.getPlayerClass();
			final int thrownBlock = 3 - (playerClass != null ? playerClass.getBossThrowReduction() : 0);
			attacker.setVelocity(new Vector(0, thrownBlock, 0));

			Common.tell(attacker, "&6The Boss " + spawnedBoss.getBoss().getName() + "thinks you could fly ;)");
		}
	}
}
