package com.jcardonne.jarvis.event;

import com.jcardonne.jarvis.PlayerCache;
import com.jcardonne.jarvis.settings.Settings;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.mineacademy.fo.Common;
import org.mineacademy.fo.FileUtil;
import org.mineacademy.fo.TimeUtil;
import org.mineacademy.fo.Valid;

public class PlayerListener implements Listener {

	@EventHandler(priority = EventPriority.LOWEST)
	public void onPlayerChatFirst(final AsyncPlayerChatEvent event) {
		Common.log("Lowest priority - " + event.getPlayer().getName() + ": " + event.getMessage());
	}

	@EventHandler(priority = EventPriority.LOW)
	public void onPlayerChatFirstD(final AsyncPlayerChatEvent event) {
		Common.log("Low priority - " + event.getPlayer().getName() + ": " + event.getMessage());
	}

	@EventHandler(priority = EventPriority.NORMAL)
	public void onPlayerChatNormal(final AsyncPlayerChatEvent event) {
		Common.log("Normal priority - " + event.getPlayer().getName() + ": " + event.getMessage());
	}

	@EventHandler(priority = EventPriority.HIGH)
	public void onPlayerChatH(final AsyncPlayerChatEvent event) {
		Common.log("High priority - " + event.getPlayer().getName() + ": " + event.getMessage());
	}

	@EventHandler(priority = EventPriority.HIGHEST)
	public void onPlayerChatHT(final AsyncPlayerChatEvent event) {
		Common.log("Highest priority - " + event.getPlayer().getName() + ": " + event.getMessage());
	}

	@EventHandler(priority = EventPriority.MONITOR)
	public void onPlayerChatM(final AsyncPlayerChatEvent event) {
		Common.log("Monitor priority - " + event.getPlayer().getName() + ": " + event.getMessage());
	}

	//--------------------------------------------------------------------------------------------------------
	@EventHandler
	public void onPlayerCommand(final PlayerCommandPreprocessEvent event) {
		final Player player = event.getPlayer();
		if (!Valid.isInListStartsWith(event.getMessage(), Settings.IgnoredCommandsToSave.IGNORED_COMMAND_TO_SAVE))
			FileUtil.write("logs/" + "commands" + ".log", "[" + TimeUtil.getFormattedDate() + "] " + player.getName() + ": " + Common.stripColors(event.getMessage()));

		if (event.getPlayer().hasPermission("plugins.bypass")) {
			return;
		}
		Common.log("Message - " + event.getMessage());

		if (event.getMessage().equalsIgnoreCase("/pl") || event.getMessage().equalsIgnoreCase("/plugins") || event.getMessage().equalsIgnoreCase("/bukkit:plugins") || event.getMessage().equalsIgnoreCase("/bukkit:pl")) {
			final String[] pluginsName = {"Jarvis", "God"};
			final String csv = String.join("&f, &a", pluginsName);
			event.setCancelled(true);
			Common.tell(event.getPlayer(), "&fPlugins (" + pluginsName.length + "&f): &a" + csv);
		}
	}
	//--------------------------------------------------------------------------------------------------------

	@EventHandler
	public void onPlayerChatColor(final AsyncPlayerChatEvent event) {
		final Player player = event.getPlayer();
		final PlayerCache cache = PlayerCache.getCache(event.getPlayer());
		final ChatColor color = cache.getColor();

		if (color != null)
			event.setMessage(color + event.getMessage());
		//Write a new line on a ""logs/" + player.getName() + ".log"" file, create if not exist
		//create a log file per player
		//FileUtil.write("logs/" + player.getName() + ".log", "[" + TimeUtil.getFormattedDate() + "] " + player.getName() + ": " + Common.stripColors(event.getMessage()));

		//create a log file for general chat from players
		FileUtil.write("logs/" + "general-messages" + ".log", "[" + TimeUtil.getFormattedDate() + "] " + player.getName() + ": " + Common.stripColors(event.getMessage()));
	}

	//--------------------------------------------------------------------------------------------------------

}
