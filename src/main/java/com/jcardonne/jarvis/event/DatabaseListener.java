package com.jcardonne.jarvis.event;

import com.jcardonne.jarvis.PlayerCache;
import com.jcardonne.jarvis.mysql.JarvisDatabase;
import lilypad.client.connect.api.event.EventListener;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerPreLoginEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.mineacademy.fo.Common;

import java.util.UUID;

public class DatabaseListener implements Listener {

	@EventListener
	public void onLogin(final AsyncPlayerPreLoginEvent event) {
		final UUID uuid = event.getUniqueId();

		if (event.getLoginResult() == AsyncPlayerPreLoginEvent.Result.ALLOWED) {
			final PlayerCache cache = PlayerCache.getCache(uuid);
			JarvisDatabase.getInstance().load(uuid,cache);
		}

	}

	@EventHandler
	public void onQuit(final PlayerQuitEvent event) {
		final Player player = event.getPlayer();
		final PlayerCache cache = PlayerCache.getCache(player.getUniqueId());

		Common.runLaterAsync(() -> JarvisDatabase.getInstance().save(player.getName(), player.getUniqueId(), cache));

	}
}
