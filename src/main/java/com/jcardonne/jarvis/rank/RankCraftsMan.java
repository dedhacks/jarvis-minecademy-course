package com.jcardonne.jarvis.rank;

import com.jcardonne.jarvis.PlayerCache;
import org.bukkit.ChatColor;
import org.bukkit.Statistic;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.mineacademy.fo.PlayerUtil;

public class RankCraftsMan extends Rank {

	protected RankCraftsMan() {
		super("Craftsman", ChatColor.WHITE);
	}

	@Override
	public Rank getNext() {
		return HERO;
	}

	@Override
	protected boolean canUpgrade(final Player player, final PlayerCache cache, final Rank rank) {
		final long zombiesKilled = PlayerUtil.getStatistic(player, Statistic.KILL_ENTITY, EntityType.ZOMBIE);
		return zombiesKilled > 10;
	}

	@Override
	protected void onUpgrade(final Player player, final Rank rank) {
		//Common.tell(player, "&6You have upgraded to the &4" + getNext().getName() + " &6rank!"); <- I add on Rank.java
	}
}
