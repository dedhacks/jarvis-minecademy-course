package com.jcardonne.jarvis.rank;

import com.jcardonne.jarvis.PlayerCache;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.mineacademy.fo.PlayerUtil;
import org.mineacademy.fo.remain.Remain;

public class RankStudent extends Rank {

	protected RankStudent() {
		super("Student", ChatColor.GRAY);
	}

	@Override
	protected boolean canUpgrade(final Player player, final PlayerCache cache, final Rank rank) {
		final long playTimeTicks = PlayerUtil.getStatistic(player, Remain.getPlayTimeStatisticName());

		final long threshold = (11 /*HOURS*/ * 60 * 60 * 20) + (40 /*MINUTES*/ * 60 * 20);
		return playTimeTicks > threshold;
	}

	@Override
	protected void onUpgrade(final Player player, final Rank rank) {
		//Common.tell(player, "&6You have upgraded to the &4" + getNext().getName() + " &6rank!"); <- I put it in upgradeToNextRank in Rank.java
	}

	@Override
	public Rank getNext() {
		return CRAFTSMAN;
	}
}
