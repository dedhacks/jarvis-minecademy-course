package com.jcardonne.jarvis.rank;

import com.jcardonne.jarvis.PlayerCache;
import lombok.Getter;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.mineacademy.fo.Common;

import java.util.HashMap;
import java.util.Map;

public abstract class Rank {

	private static final Map<String, Rank> byName = new HashMap<>();

	public static final Rank STUDENT = new RankStudent();
	public static final Rank CRAFTSMAN = new RankCraftsMan();
	public static final Rank HERO = new RankHero();
	public static final Rank MASTER = new RankMaster();

	@Getter
	private final String name;

	@Getter
	private final ChatColor color;

	protected Rank(final String name, final ChatColor color) {
		this.name = name;
		this.color = color;

		byName.put(name, this);
	}

	public Rank getNext() {
		return null;
	}

	public boolean upgradeToNextRank(final Player player) {
		return upgradeToNextRank(player, false);
	}

	public boolean upgradeToNextRank(final Player player, final boolean force) {
		final Rank nextRank = getNext();

		if (nextRank == null)
			return false;

		final PlayerCache cache = PlayerCache.getCache(player);

		if (canUpgrade(player, cache, nextRank) || force) {
			cache.setRank(nextRank);
			onUpgrade(player, nextRank);

			Common.tell(player, "&6You have upgraded to the &4" + getNext().getName() + " &6rank!");
			return true;
		}

		return false;
	}

	protected boolean canUpgrade(final Player player, final PlayerCache cache, final Rank rank) {
		return false;
	}

	protected void onUpgrade(final Player player, final Rank rank) {
	}

	public static final Rank getFirstRank() {
		return STUDENT;
	}

	public static final Rank getByName(final String name) {
		return byName.get(name);
	}
}
