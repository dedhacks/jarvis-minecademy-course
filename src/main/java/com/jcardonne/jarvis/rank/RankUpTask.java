package com.jcardonne.jarvis.rank;

import com.jcardonne.jarvis.PlayerCache;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;
import org.mineacademy.fo.remain.Remain;

public class RankUpTask extends BukkitRunnable {
	@Override
	public void run() {
		for (final Player player : Remain.getOnlinePlayers()) {
			final PlayerCache cache = PlayerCache.getCache(player);

			cache.getRank().upgradeToNextRank(player);
		}
	}
}
