package com.jcardonne.jarvis.rank;

import org.bukkit.ChatColor;

public class RankHero extends Rank {

	protected RankHero() {
		super("Hero", ChatColor.BLUE);
	}

	@Override
	public Rank getNext() {
		return MASTER;
	}
}
