package com.jcardonne.jarvis.settings;

import org.mineacademy.fo.model.Replacer;
import org.mineacademy.fo.settings.SimpleLocalization;

public class Localization extends SimpleLocalization {

	@Override
	protected int getConfigVersion() {
		return 1;
	}

	public static class Boarding {
		public static String CONVERSATION_CANCELLED;


		private static void init() {
		pathPrefix("Boarding");

		CONVERSATION_CANCELLED = getString("Cancelled");
		}
	}

	public static class Command {

		public static class Boss {
			public static Replacer NOT_FOUND;


			private static void init() {
				pathPrefix("Command.Boss");
				NOT_FOUND = getReplacer("Not_Found");
			}

		}
		public static String CONVERSATION_CANCELLED;


		private static void init() {
			pathPrefix("Boarding");

			CONVERSATION_CANCELLED = getString("Cancelled");
		}
	}
}
