package com.jcardonne.jarvis.settings;

import org.mineacademy.fo.model.SimpleTime;
import org.mineacademy.fo.settings.SimpleSettings;

import java.util.List;

public class Settings extends SimpleSettings {

	@Override
	protected int getConfigVersion() {
		return 1;
	}

	public static class Menu {
		public static String MENU_PREFERENCES_TITLE;
		public static String MENU_EGGS_TITLE;
		public static String MENU_CHATCOLOR_TITLE;
		public static String MENU_JARVIS_TOOLS_TITLE;

		private static void init() {
			pathPrefix("Menu");

			MENU_PREFERENCES_TITLE = getString("Preferences_Title");
			MENU_EGGS_TITLE = getString("Eggs_Title");
			MENU_CHATCOLOR_TITLE = getString("ChatColor_Title");
			MENU_JARVIS_TOOLS_TITLE = getString("Tools_Menu_Title");
		}
	}

	public static class Join {
		public static Boolean GIVE_STUFF;

		private static void init() {
			pathPrefix("Join");
			GIVE_STUFF = getBoolean("Give_Stuff");
		}
	}

	public static class Explode {
		public static Boolean EXPLODE_COW;
		public static Double EXPLODE_POWER;

		private static void init() {
			pathPrefix("Explode");
			EXPLODE_COW = getBoolean("Cow");
			EXPLODE_POWER = getDoubleSafe("Power");
		}
	}

	public static class Prefix {
		public static String PREFIX;

		private static void init() {
			pathPrefix(null);
			PREFIX = getString("Prefix");
		}
	}

	public static class IgnoredCommandsToSave {
		public static List<String> IGNORED_COMMAND_TO_SAVE;

		private static void init() {
			pathPrefix(null);
			IGNORED_COMMAND_TO_SAVE = getStringList("Ignored_Log_Commands");
		}
	}

	public static class Broadcast {
		public static SimpleTime TIMER;

		private static void init() {
			pathPrefix("Broadcast");
			TIMER = getTime("Delay");
		}
	}
}