package com.jcardonne.jarvis.tools;

import com.jcardonne.jarvis.utils.JarvisUtil;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockExplodeEvent;
import org.bukkit.event.entity.ProjectileHitEvent;
import org.bukkit.event.entity.ProjectileLaunchEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.util.Vector;
import org.mineacademy.fo.BlockUtil;
import org.mineacademy.fo.ItemUtil;
import org.mineacademy.fo.RandomUtil;
import org.mineacademy.fo.menu.model.ItemCreator;
import org.mineacademy.fo.menu.tool.Tool;
import org.mineacademy.fo.remain.CompMaterial;

import java.util.HashMap;
import java.util.Map;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class KittyBow extends Tool implements Listener {


	private final Map<Location, Vector> explodedBlocksLocations = new HashMap<>();

	@Getter
	private final static Tool instance = new KittyBow();

	@Override
	public ItemStack getItem() {
		return ItemCreator.of(CompMaterial.BOW, "&bExplosive Arrow").unbreakable(true).glow(true).build().make();
	}

	@EventHandler
	public void onProjectileLaunch(final ProjectileLaunchEvent event) {
		final Projectile projectile = event.getEntity();

		if (!(projectile.getShooter() instanceof Player))
			return;

		final Player player = (Player) projectile.getShooter();

		if (!ItemUtil.isSimilar(player.getItemInHand(), getItem()))
			return;

		if (!JarvisUtil.checkKittyArrow(player, event))
			event.setCancelled(true);
		return;

		//CompMetadata.setMetadata(projectile, "ArrowKitty");
	}


	@EventHandler
	public void onProjectileHit(final ProjectileHitEvent event) {
		/*if (!CompMetadata.hasMetadata(event.getEntity(), "KittyArrow"))
			return;*/

		Projectile projectile = event.getEntity();
		if (!(projectile.getShooter() instanceof Player))
			return;

		explodedBlocksLocations.put(projectile.getLocation().getBlock().getLocation(), projectile.getVelocity());

		projectile.remove();
		projectile.getWorld().createExplosion(projectile.getLocation(), 4F);
	}

	@EventHandler
	public void onBlockExplode(BlockExplodeEvent event) {
		Vector vector = explodedBlocksLocations.remove(event.getBlock().getLocation());

		if (vector != null) {
			for (Block block : event.blockList()) {
				if (RandomUtil.chanceD(0.50D)) //50% chances that blocks fly
					BlockUtil.shootBlock(block, vector); // can Use a third arg if you want the burnable block take fire by chances
				else
					block.setType(CompMaterial.AIR.getMaterial());

				event.setYield(0F); //prevent minimize drop
			}
		}
	}

	@Override
	protected void onBlockClick(final PlayerInteractEvent event) {

	}
}
