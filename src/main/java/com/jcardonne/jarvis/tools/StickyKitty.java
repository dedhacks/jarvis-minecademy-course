package com.jcardonne.jarvis.tools;

import com.jcardonne.jarvis.utils.JarvisUtil;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.bukkit.entity.Ocelot;
import org.bukkit.entity.Player;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.mineacademy.fo.EntityUtil;
import org.mineacademy.fo.menu.model.ItemCreator;
import org.mineacademy.fo.menu.tool.Tool;
import org.mineacademy.fo.remain.CompMaterial;
import org.mineacademy.fo.remain.CompParticle;
import org.mineacademy.fo.remain.CompSound;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class StickyKitty extends Tool {


	@Getter
	private final static Tool instance = new StickyKitty();

	@Override
	public ItemStack getItem() {
		return ItemCreator.of(CompMaterial.STICK, "&6STICKY Cannon").unbreakable(true).glow(true).build().make();

	}

	@Override
	protected void onBlockClick(final PlayerInteractEvent event) {
		if (event.getAction() != Action.RIGHT_CLICK_AIR)
			return;

		Player player = event.getPlayer();
		if (!JarvisUtil.checkKittyArrow(player, event))
			return;

		Ocelot ocelot = player.getWorld().spawn(player.getEyeLocation(), Ocelot.class);
		ocelot.setVelocity(player.getEyeLocation().getDirection().multiply(4.0D));

		EntityUtil.trackFlying(ocelot, () -> {
			CompParticle.END_ROD.spawn(ocelot.getLocation());
		});

		EntityUtil.trackFalling(ocelot, () -> {
			ocelot.remove();
			ocelot.getWorld().createExplosion(ocelot.getLocation(), 4F);
			CompSound.CAT_HISS.play(ocelot.getLocation());
		});
	}

	@Override
	protected boolean ignoreCancelled() {
		return false;
	}
}
