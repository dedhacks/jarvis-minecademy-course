package com.jcardonne.jarvis.tools;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.mineacademy.fo.menu.model.ItemCreator;
import org.mineacademy.fo.menu.tool.Tool;
import org.mineacademy.fo.remain.CompMaterial;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class KittyAmmo extends Tool {

	@Getter
	private final static Tool instance = new KittyAmmo();

	@Override
	public ItemStack getItem() {
		return ItemCreator.of(CompMaterial.ARROW, "&6Kitty Ammo").unbreakable(true).glow(true).build().make();
	}

	@Override
	protected void onBlockClick(final PlayerInteractEvent event) {
		event.setCancelled(true);
	}
}
