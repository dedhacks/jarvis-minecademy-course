package com.jcardonne.jarvis.tools;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.mineacademy.fo.Common;
import org.mineacademy.fo.menu.model.ItemCreator;
import org.mineacademy.fo.menu.tool.Tool;
import org.mineacademy.fo.remain.CompMaterial;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class DiamondAxeTool extends Tool {

	@Getter
	private static final Tool instance = new DiamondAxeTool();

	@Override
	public ItemStack getItem() {
		return ItemCreator.of(CompMaterial.DIAMOND_AXE, "The Magical Axe", "Click to turn any", "blocks to a diamond block!").glow(true).build().make();
	}

	@Override
	protected void onBlockClick(final PlayerInteractEvent event) {
		Common.log("Action - " + event.getAction());
		if (event.getAction() == Action.RIGHT_CLICK_BLOCK)
			event.getClickedBlock().setType(CompMaterial.DIAMOND_BLOCK.getMaterial());
		Common.tell(event.getPlayer(), "The clicked block has been transformed!");
	}
}
