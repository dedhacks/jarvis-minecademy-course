package com.jcardonne.jarvis.tools;

import lombok.Getter;
import org.bukkit.entity.Fireball;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.inventory.ItemStack;
import org.mineacademy.fo.Common;
import org.mineacademy.fo.menu.model.ItemCreator;
import org.mineacademy.fo.menu.tool.Rocket;
import org.mineacademy.fo.menu.tool.Tool;
import org.mineacademy.fo.remain.CompMaterial;
import org.mineacademy.fo.remain.CompParticle;

public class HellFireRocket extends Rocket {

	@Getter
	private static final Tool instance = new HellFireRocket();

	private HellFireRocket() {
		super(Fireball.class, 2F, 10F, true);
	}

	@Override
	public ItemStack getItem() {
		return ItemCreator.of(CompMaterial.FIRE_CHARGE, "&cHellFire", "", "Deliver").build().make();
	}

	@Override
	protected void onLaunch(final Projectile projectile, final Player shooter) {
		Common.tell(shooter, "&8[&aRocket&8] &7Hey Grandma there is some cookies for ya!");
	}

	@Override
	protected void onExplode(final Projectile projectile, final Player shooter) {
		Common.tell(shooter, "&8[&aRocket&8] &7Delivered, grandma not responding yet...");
	}

	@Override
	protected void onFlyTick(final Projectile projectile, final Player shooter) {
		CompParticle.FIREWORKS_SPARK.spawn(projectile.getLocation(), 1D);
	}
}
